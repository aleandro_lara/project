FROM alpine:latest
ADD /src/main/java/com/prueba/prueba/BackendPruebaApplication.java /var/tmp/BackendPruebaApplication.java
RUN apk --update add openjdk8-jre
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "BackendPruebaApplication"]

